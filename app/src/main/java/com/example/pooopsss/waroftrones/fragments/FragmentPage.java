package com.example.pooopsss.waroftrones.fragments;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.pooopsss.waroftrones.R;
import com.example.pooopsss.waroftrones.adapters.HomersAdapters;
import com.example.pooopsss.waroftrones.database.UserListDB;
import com.example.pooopsss.waroftrones.util.managers.ConstantManager;
import com.example.pooopsss.waroftrones.util.managers.DataManager;
import com.example.pooopsss.waroftrones.util.network.res.HourseInfo;
import com.example.pooopsss.waroftrones.util.network.res.HourseUsers;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class FragmentPage extends Fragment {

    private ArrayList<HourseUsers> mHoursesUsers = new ArrayList<>();
    private HomersAdapters mAdapter;
    public int HomeID;
    private RecyclerView recyclerView;
    DataManager mDataManager;



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_for_list,
                container, false);

        mDataManager = DataManager.getInstance();
        if(savedInstanceState == null){

        } else {

        }

       mHoursesUsers = UserListDB.getUsersOnHome(HomeID, this.getContext());


        HomersAdapters saveAdapter = new HomersAdapters(mHoursesUsers);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recucle_view);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(container.getContext());
        recyclerView.setAdapter(saveAdapter);
        recyclerView.setLayoutManager(linearLayoutManager);
        return  rootView;
    }

    public FragmentPage(){

    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt("HomeID", this.HomeID);
        super.onSaveInstanceState(outState);
    }



    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(savedInstanceState == null){
            Log.i("TTTT","SAVED NULL");
        } else {
            HomeID = savedInstanceState.getInt("HomeID", 0);
            
        }

    }
}