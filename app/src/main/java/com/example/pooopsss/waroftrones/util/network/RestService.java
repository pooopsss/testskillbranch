package com.example.pooopsss.waroftrones.util.network;

import com.example.pooopsss.waroftrones.util.managers.ConstantManager;
import com.example.pooopsss.waroftrones.util.network.req.HourseInfoReq;
import com.example.pooopsss.waroftrones.util.network.res.HourseInfo;
import com.example.pooopsss.waroftrones.util.network.res.HourseUsers;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by pooopsss on 16.10.2016.
 */

public interface RestService {
    @GET("houses/{param}")
    Call<HourseInfo> hourseInfo(@Path(value = "param", encoded = true) int param);

    @GET("characters/{param}")
    Call<HourseUsers> hourseUser(@Path(value = "param", encoded = true) int param);

    @GET("{param}")
    Call<HourseUsers> customUser(@Path(value = "param", encoded = true) String param);

}
