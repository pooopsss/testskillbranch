package com.example.pooopsss.waroftrones;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;

import com.example.pooopsss.waroftrones.util.managers.ConstantManager;
import com.example.pooopsss.waroftrones.util.managers.DataManager;

/**
 * Created by pooopsss on 16.10.2016.
 */

public class SplashActivity extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);


        checkOrLoadLinkToDB();
    }



    protected void checkOrLoadLinkToDB(){
        showProgress();
        DataManager mDataManager = DataManager.getInstance();
        mDataManager.ClearHomeUsers(this);
        mDataManager.setHoursUsersLinks( ConstantManager.HOME_TARGARYENS_ID,this);
        mDataManager.setHoursUsersLinks( ConstantManager.HOME_STARKS_ID,this);
        mDataManager.setHoursUsersLinks( ConstantManager.HOME_LANISTER_ID,this);

        final Handler handler  = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                loadMainWindow();
            }
        },5000);
    }



    protected void loadMainWindow(){
        hideProgress();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
