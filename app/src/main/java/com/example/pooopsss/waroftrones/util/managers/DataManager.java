package com.example.pooopsss.waroftrones.util.managers;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.pooopsss.waroftrones.adapters.HomersAdapters;
import com.example.pooopsss.waroftrones.database.UserListDB;
import com.example.pooopsss.waroftrones.util.network.RestService;
import com.example.pooopsss.waroftrones.util.network.ServiceGenerator;
import com.example.pooopsss.waroftrones.util.network.req.HourseInfoReq;
import com.example.pooopsss.waroftrones.util.network.res.HourseInfo;
import com.example.pooopsss.waroftrones.util.network.res.HourseUsers;

import java.io.Console;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by pooopsss on 16.10.2016.
 */

public class DataManager {

    private static DataManager INSTANCE = null;

    private RestService mRestService;

    public DataManager(){
        this.mRestService = ServiceGenerator.createService(RestService.class);
    }

    public static DataManager getInstance(){
        if(INSTANCE == null){
            INSTANCE = new DataManager();
        }
        return INSTANCE;
    }

    public Call<HourseInfo> getHoursInfo(int HomeID){
        return mRestService.hourseInfo(HomeID);
    }

    public Call<HourseUsers> getHoursUser(int userID){
        return mRestService.hourseUser(userID);
    }

    public Call<HourseUsers> getHoursUserUrl(String url){
        return mRestService.customUser(url);
    }




    public void setHoursUsersLinks(final int hourseNumber, final Context context){
        Call<HourseInfo> h = this.getHoursInfo(hourseNumber);
        h.enqueue(new Callback<HourseInfo>() {
            @Override
            public void onResponse(Call<HourseInfo> call, Response<HourseInfo> response) {
                try {
                    if(response.body().swornMembers.size()>0){
                       insertHoumeUsers(context,(ArrayList<String>) response.body().swornMembers, hourseNumber );
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<HourseInfo> call, Throwable t) {

            }
        });
    }


    public void ClearHomeUsers(Context context){
        UserListDB dbHelper = new UserListDB(context);
        final SQLiteDatabase database = dbHelper.getWritableDatabase();
        database.execSQL("DELETE FROM "+ConstantManager.TABLE_USERS);
    }

    public void insertHoumeUsers(Context context, ArrayList<String> userlist, final int HomeID){
        if(userlist.size() > 0){
            UserListDB dbHelper = new UserListDB(context);
            final SQLiteDatabase database = dbHelper.getWritableDatabase();

            for(String item: userlist){
                Call<HourseUsers> h = this.getHoursUserUrl(item);
                h.enqueue(new Callback<HourseUsers>() {
                    @Override
                    public void onResponse(Call<HourseUsers> call, Response<HourseUsers> response) {
                        try {
                            ContentValues contentValues = new ContentValues();
                            contentValues.put(UserListDB.KEY_HOME__ID, String.valueOf(HomeID));
                            contentValues.put(UserListDB.KEY_NAME, response.body().name);
                            contentValues.put(UserListDB.KEY_GENDER, response.body().gender);
                            database.insert(ConstantManager.TABLE_USERS, null, contentValues);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<HourseUsers> call, Throwable t) {

                    }
                });

            }
        }
    }
}
