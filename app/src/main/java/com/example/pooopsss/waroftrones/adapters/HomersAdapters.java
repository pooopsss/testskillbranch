package com.example.pooopsss.waroftrones.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.pooopsss.waroftrones.R;
import com.example.pooopsss.waroftrones.util.network.res.HourseInfo;
import com.example.pooopsss.waroftrones.util.network.res.HourseUsers;

import java.util.ArrayList;
import java.util.zip.Inflater;

/**
 * Created by pooopsss on 16.10.2016.
 */

public class HomersAdapters extends RecyclerView.Adapter<HomersAdapters.UserViewHolder> {
    ArrayList<HourseUsers> homes;
    Context context;

    public HomersAdapters( ArrayList<HourseUsers> list){
        homes = list;
    }


    @Override
    public HomersAdapters.UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View converterView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item, parent, false);
        this.context = parent.getContext();
        return new UserViewHolder(converterView);
    }

    @Override
    public void onBindViewHolder(HomersAdapters.UserViewHolder holder, int position) {
        HourseUsers hItem = homes.get(position);
        holder.user_name.setText(hItem.name);
        holder.user_descr.setText(hItem.gender);
    }

    @Override
    public int getItemCount() {
        return homes.size();
    }

    public class UserViewHolder extends RecyclerView.ViewHolder {
        protected ImageView imageView;
        protected TextView user_name;
        protected TextView user_descr;

        public UserViewHolder(View itemView) {
            super(itemView);

            imageView = (ImageView) itemView.findViewById(R.id.person_photo);
            user_name = (TextView) itemView.findViewById(R.id.person_name);
            user_descr = (TextView) itemView.findViewById(R.id.person_descr);
        }


    }
}
