package com.example.pooopsss.waroftrones.util.managers;

/**
 * Created by pooopsss on 16.10.2016.
 */

public interface ConstantManager {
    String HOME_STARKS = "STARKS";
    String HOME_LANISTER = "LANISTER";
    String HOME_TARGARYENS = "TARGARYENS";

    int HOME_STARKS_ID=362;
    int HOME_LANISTER_ID=229;
    int HOME_TARGARYENS_ID=378;


    String DATABASE_NAME = "waroftrones";
    String TABLE_USERS = "users";
}
