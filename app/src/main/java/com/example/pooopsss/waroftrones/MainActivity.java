package com.example.pooopsss.waroftrones;

import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.graphics.drawable.DrawableWrapper;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;

import com.example.pooopsss.waroftrones.adapters.HomersAdapters;
import com.example.pooopsss.waroftrones.adapters.ListAdapter;
import com.example.pooopsss.waroftrones.util.NetworkStatusChecker;
import com.example.pooopsss.waroftrones.util.managers.ConstantManager;
import com.example.pooopsss.waroftrones.util.managers.DataManager;
import com.example.pooopsss.waroftrones.util.network.req.HourseInfoReq;
import com.example.pooopsss.waroftrones.util.network.res.HourseInfo;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MainActivity extends BaseActivity {

    private CoordinatorLayout mCoordLayout;
    private final int TIMEOUT_LOADING_TIME = 5000;
    DataManager mDataManager;
    TabLayout tabLayout;
    Toolbar toolbar;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mDataManager = DataManager.getInstance();

        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setupToolbar();

        ViewPager viewPager = (ViewPager) findViewById(R.id.view_pager);
        viewPager.setAdapter(new ListAdapter(getSupportFragmentManager(),this));
        viewPager.setCurrentItem(0);


        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setTabMode(TabLayout.MODE_FIXED);
        tabLayout.setupWithViewPager(viewPager);


        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                Log.i("XYI", tab.getText().toString());

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        mCoordLayout = (CoordinatorLayout) findViewById(R.id.main_coord);

//        Call<HourseInfo> h = mDataManager.getHoursInfo(ConstantManager.HOME_STARKS_ID);
//        h.enqueue(new Callback<HourseInfo>() {
//            @Override
//            public void onResponse(Call<HourseInfo> call, Response<HourseInfo> response) {
//                try {
//                    String name = response.body().name;
//                    mHourses.add(response.body());
//                    mAdapter = new HomersAdapters(mHourses);
//                    //mHourses = (ArrayList<HourseInfo>) response.body());
//
//                  //  String status = response.body().getWeather().get(0).getDescription();
////
////                    String humidity = response.body().getMain().getHumidity().toString();
////
////                    String pressure = response.body().getMain().getPressure().toString();
////
////                    txt_city.setText("city  :  " + city);
////                    txt_status.setText("status  :  " + status);
////                    txt_humidity.setText("humidity  : " + humidity);
////                    txt_pressure.setText("pressure  :  " + pressure);
//                    //Log.d("XYI", name);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<HourseInfo> call, Throwable t) {
//                showSnackbar(t.getMessage());
//            }
//        });


    }

    public void setupToolbar(){
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if(actionBar != null){
            actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_black_24dp);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    private void showSnackbar(String message){
        Snackbar.make(mCoordLayout,message,Snackbar.LENGTH_LONG).show();
    }

}
