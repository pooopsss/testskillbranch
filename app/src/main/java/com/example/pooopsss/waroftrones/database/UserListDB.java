package com.example.pooopsss.waroftrones.database;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.pooopsss.waroftrones.util.managers.ConstantManager;
import com.example.pooopsss.waroftrones.util.network.res.HourseUsers;

import java.util.ArrayList;

public class UserListDB extends SQLiteOpenHelper  {
    public static final String KEY_ID = "_id";
    public static final String KEY_HOME__ID = "home_id";
    public static final String KEY_NAME = "name";
    public static final String KEY_GENDER = "gender";
    public static final int DATABASE_VERSION = 1;

    public UserListDB(Context context) {
        super(context, ConstantManager.DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + ConstantManager.TABLE_USERS + "(" + KEY_ID
                + " integer primary key," + KEY_HOME__ID + " integer," + KEY_NAME + " text," + KEY_GENDER + " text" + ")");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists " + ConstantManager.TABLE_USERS);

        onCreate(db);
    }

    public static ArrayList<HourseUsers> getUsersOnHome(int HomeID, Context context){
        ArrayList<HourseUsers> list = new ArrayList<HourseUsers>();
        UserListDB dbHelper;
        SQLiteDatabase db;
        dbHelper  = new UserListDB(context);

        db = dbHelper.getWritableDatabase();
        String selection = UserListDB.KEY_HOME__ID+" = ?";
        String[] selectionArgs = new String[] { String.valueOf(HomeID) };

        Cursor c = db.query(ConstantManager.TABLE_USERS, null, selection, selectionArgs, null, null, "name");
        if (c.moveToFirst()) {
            int idIndex = c.getColumnIndex(UserListDB.KEY_ID);
            int nameIndex = c.getColumnIndex(UserListDB.KEY_NAME);
            int genderIndex = c.getColumnIndex(UserListDB.KEY_GENDER);
            do {
                HourseUsers hUser = new HourseUsers();
                hUser.name = c.getString(nameIndex);
                hUser.gender = c.getString(genderIndex);
                list.add(hUser);
            } while (c.moveToNext());
        } else {
            Log.d("LOGDB", "0 rows");
        }
        c.close();
        return list;
    }


}
