package com.example.pooopsss.waroftrones.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.pooopsss.waroftrones.R;
import com.example.pooopsss.waroftrones.fragments.FragmentPage;
import com.example.pooopsss.waroftrones.util.managers.ConstantManager;

/**
 * Created by pooopsss on 16.10.2016.
 */

public class ListAdapter extends FragmentPagerAdapter {
    private String[] tabstring = new String[]{ConstantManager.HOME_STARKS,ConstantManager.HOME_LANISTER,ConstantManager.HOME_TARGARYENS};
    Context context;

    public ListAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {
        int HomeID = 0;
        switch (tabstring[position]){
            case ConstantManager.HOME_STARKS:
                HomeID  = ConstantManager.HOME_STARKS_ID;
                break;
            case ConstantManager.HOME_LANISTER:
                HomeID  = ConstantManager.HOME_LANISTER_ID;
                break;
            case ConstantManager.HOME_TARGARYENS:
                HomeID  = ConstantManager.HOME_TARGARYENS_ID;
                break;
        }
        Log.i("XYI","______________GET _ITEM____"+String.valueOf(position));
        FragmentPage fragmentPage = new FragmentPage();
        fragmentPage.HomeID = HomeID;
        return fragmentPage;
    }

    @Override
    public int getCount() {
        return this.tabstring.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        //return super.getPageTitle(position);
        return this.tabstring[position];
    }


}
