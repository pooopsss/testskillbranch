package com.example.pooopsss.waroftrones;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by pooopsss on 16.10.2016.
 */

public class BaseActivity extends AppCompatActivity {

    protected ProgressDialog mProgressDialog;
    static final String TAG = "BaseActivity";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    public void showProgress(){
        if(mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this, R.style.custom_dialog);
            mProgressDialog.setCancelable(false);
            mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        mProgressDialog.show();
        mProgressDialog.setContentView(R.layout.progress_bar);
    }

    public void hideProgress(){
        if(mProgressDialog != null) {
            if(mProgressDialog.isShowing())
                mProgressDialog.hide();
        }
    }

    public void showError(String message, Exception error){
        showToast(message);
        Log.e(TAG, String.valueOf(error));
    }

    public void showToast(String message){
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
    public void showToastLong(String message){
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }
}
